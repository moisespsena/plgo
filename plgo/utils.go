package main

import (
	"bytes"
	"strings"
)

type strCase bool

const (
	lower strCase = false
	upper strCase = true
)

// ToDbName convert string to db name
func ToDbName(value string) string {
	var (
		buf bytes.Buffer
		lastCase,
		currCase,
		nextCase,
		nextNumber strCase
	)

	for i, v := range value[:len(value)-1] {
		nextCase = value[i+1] >= 'A' && value[i+1] <= 'Z'
		nextNumber = value[i+1] >= '0' && value[i+1] <= '9'

		if v == '-' {
			v = '_'
			currCase = lower
			buf.WriteRune(v)
		} else if i > 0 {
			if currCase == upper {
				if lastCase == upper && (nextCase == upper || nextNumber == upper) {
					buf.WriteRune(v)
				} else {
					if value[i-1] != '_' && value[i+1] != '_' {
						buf.WriteRune('_')
					}
					buf.WriteRune(v)
				}
			} else {
				buf.WriteRune(v)
				if i == len(value)-2 && (nextCase == upper && nextNumber == lower) {
					buf.WriteRune('_')
				}
			}
		} else {
			currCase = upper
			buf.WriteRune(v)
		}
		lastCase = currCase
		currCase = nextCase
	}

	buf.WriteByte(value[len(value)-1])

	return strings.ToLower(buf.String())
}
